import React from 'react'
import './App.css'
import {useState,useEffect} from 'react'
function App() {
  const [position, setPosition] = useState({x: 0, y: 0});
  const [hover,setHover]=useState(false)
     useEffect(() => {
         addEventListeners();
         return () => removeEventListeners();
     }, []);
  
     const addEventListeners = () => {
       document.addEventListener("mousemove", onMouseMove);
     };
  
    const removeEventListeners = () => {
         document.removeEventListener("mousemove", onMouseMove);
     };
  
     const onMouseMove = (e) => {
        setPosition({x: e.clientX, y: e.clientY});
        console.log(e)
    };                                                               
  
 
  return (
    <div className="App">
      <div className={hover? 'cursor_hover' : 'cursor'} style={{
        left: `${position.x}px`,
        top: `${position.y}px`
      }}>
      </div>
      <h1  onMouseEnter={() => setHover(true)}
             onMouseLeave={() => setHover(false)}>React Js Interview</h1>
    </div>
  );
}

export default App;
