import React from 'react'
import './App.css'
import {useState,useEffect} from 'react'
import { motion, useMotionValue,useSpring } from "framer-motion"

function App() {
 // const [position, setPosition] = useState({x: 0, y: 0});
  const [hover,setHover]=useState(false)
  const[secondhover,setSecondhover]=useState(false)
  const cursorX = useMotionValue(0)
  const cursorY = useMotionValue(0)
 const springConfig = { damping: 25, stiffness: 700 };
  const cursorXSpring = useSpring(cursorX, springConfig);
 const cursorYSpring = useSpring(cursorY, springConfig);
     useEffect(() => {
         addEventListeners();
         return () => removeEventListeners();
     }, []);
  
     const addEventListeners = () => {
       document.addEventListener("mousemove", onMouseMove);
     };
  
    const removeEventListeners = () => {
         document.removeEventListener("mousemove", onMouseMove);
     };
  
     const onMouseMove = (e) => {
      cursorX.set(e.clientX)
      cursorY.set(e.clientY)
      // setPosition({x: e.clientX, y: e.clientY});
        //console.log(e)
    };                                                               
  
 
  return (
    <div className="App">
      <motion.div className={`${ hover? 'cursor_hover' : 'cursor'}
                         ${ secondhover? 'secondcursor_hover' : 'cursor'}
                       `} 
                       style={{
                       left:cursorXSpring,
                        top:cursorYSpring,
                     // left: `${position.x}px`,
                       //top: `${position.y}px`
                      }}
          />
      
      <h1  onMouseEnter={() => setHover(true)}
           onMouseLeave={() => setHover(false)}>
             <span>ReactJs </span>
             <span onMouseEnter={() => setSecondhover(true)}
                   onMouseLeave={() => setSecondhover(false)} class="interview">
                   Interview
                   </span>
       </h1>
    </div>
  );
}

export default App;
